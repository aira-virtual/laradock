#!/bin/bash

ICINGA_HOSTNAME="icinga2"
SLACK_WEBHOOK_URL="https://hooks.slack.com/services/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"
SLACK_CHANNEL="#monitoreo"
SLACK_BOTNAME="icinga2"

#Set the message icon based on ICINGA service state
if [ "$SERVICESTATE" = "CRITICAL" ]
then
    ICON=":bomb:"
elif [ "$SERVICESTATE" = "WARNING" ]
then
    ICON=":warning:"
elif [ "$SERVICESTATE" = "OK" ]
then
    ICON=":beer:"
elif [ "$SERVICESTATE" = "UNKNOWN" ]
then
    ICON=":question:"
else
    ICON=":white_medium_square:"
fi

#Send message to Slack
PAYLOAD="payload={\"channel\": \"${SLACK_CHANNEL}\", \"username\": \"${SLACK_BOTNAME}\", \"text\": \"${ICON} HOST: ${HOSTNAME}|${HOSTDISPLAYNAME}   SERVICE: ${SERVICEDESC}|${SERVICEDISPLAYNAME}  STATE: ${SERVICESTATE}\"}"

curl --connect-timeout 30 --max-time 60 -s -S -X POST --data-urlencode "${PAYLOAD}" "${SLACK_WEBHOOK_URL}"
