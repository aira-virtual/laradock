#!/bin/bash

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $BASEDIR

SCRIPTS=$BASEDIR/scripts/*

time=0
for script in $SCRIPTS
do
	filename=`basename $script`
	context=`echo $filename | sed -E 's/[0-9]+-//g'`
	next_time=`echo $filename | sed -E 's/([0-9]+)-.*/\1/g'`
	wait_time=$((next_time - time))
	echo "Waiting: $wait_time seconds"
	sleep $wait_time
	time=$next_time
	echo "Creating context: $context"
	screen -d -m -S $context
	n=0
	while read -r command
	do
	    echo "Executing $command at screen $n"
	    if [ "$n" -gt "0" ]; then
  	        screen -S $context -X screen $n
	    fi
  	    screen -S $context -p $n -X stuff "$command"
  	    screen -S $context -p $n -X stuff $'\012'
	    n=$((n+1))
	done < "$script"
done

exit 0
