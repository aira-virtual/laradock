# LaraDock

![](https://s31.postimg.org/nbettdki3/lara_dock_poster_new.jpg)

Proyecto original [Laradock](https://github.com/LaraDock/laradock) 

Si ya estas usando laradock debes seguir los siguientes pasos: 

1. Si aun sigues usando la version original de laradock detenerla con
```bash
docker-compose stop
```

2. Clonar el repositiorio de laradock de gitlab (fuera de la carpeta del proyecto aira):
```bash
git clone git@gitlab.com:PractiJob/laradock.git
```

3. Ingresar al directorio laradock y hacer un rebuild de los contenedores principales:
```bash
docker-compose build nginx mysql php-fpm workspace
```

4. Iniciar el nuevo entorno basico:
```bash
docker-compose up -d nginx mysql elk
```

